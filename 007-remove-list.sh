#!/usr/bin/env bash

############################################################
# Makes the script more portable ###########################
readonly DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
############################################################

readonly rmlist="${DIR}/remove-list.txt"


# sudo pacman -R - < $rmlist

yay -R    - <  $rmlist		


figlet -c -f smslant *REMOVED*


figlet -c -f smslant ${USER}



