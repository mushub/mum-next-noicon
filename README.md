### MUM-Pro-noicon. 
<pre><code>PORTABLE VERSION >>></code></pre>


> ![ArcoLinux_2020-11-29_11-49-02](/uploads/997d70232a143231b952ad8aaad4ceea/ArcoLinux_2020-11-29_11-49-02.png)

### How to run MUM ::

> ###### `$ git clone https://gitlab.com/mushub/mum-pro-noicon.git`

### Run from MUM-Pro folder, anyware on your system.
> ###### `$ ./update-manager`

#### - --- Before doing anything with MUM :: DO :: cd to mum-pro-noicon folder and make scripts executable.


> ###### `$ chmod +x 004-add-install-list.sh 005-add-remove-list.sh 007-remove-list.sh 008-install-list.sh remove.sh search.sh update-manager`


### This is needed to give MUM the basic to run. 
> ###### `$ sudo pacman -S yay terminator konsole figlet `


- ###### Terminator is for MUM
- ###### Konsole and yay _:: needed for installing deps  ::_
- ###### Figlet for fancy comments


------------------------------------------------------------------------------------------

#### Terminator works nicely. Easy to setup.
###### `$ bash ~/mum-pro-noicon/update-manager`


## **RUN MUM and finish installing deps**
> ##### - --- Go to install packlist button in the MUM menu and run the script to install all other depenties. logout/in.
> ##### - --- MUM needs to refresh to see changes.


 ⚠ ::_ JUDGE AND CHECK :: INSTALL-LIST & REMOVE-LIST_ :: ⚠


#### Source script ::

> https://askubuntu.com/questions/1705/how-can-i-create-a-select-menu-in-a-shell-script
-------------------------------------------------------------------------------------------------------------------------

#### Video demo MUM-Next-noicon and arcolinux xfce :: 
> https://d.tube/v/linux-bird-020/QmXfPD47Kn5Rsz2t5Jxk9XXkyM6dHACmhQHJNQku4WraHU

##### scrot mum-pro-noicon :: arcolinux xfce ::
> ![ArcoLinux_2020-11-29_11-57-45-goed](/uploads/ac3cd70fd994f4a0bb657e1457bf5721/ArcoLinux_2020-11-29_11-57-45-goed.png)
######  Todo : Check/Clean readme :: 

